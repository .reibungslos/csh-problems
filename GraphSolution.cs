using System.Collections.Generic;
using System.Linq;

namespace leetc1
{
    public class GraphSolution
    {
        public int[] FindRedundantConnection(int[][] edges)
        {
            var res = new int[2];
            var gg = new Dictionary<int, List<int>>();
            var set = new HashSet<int>();

            bool Domoo(Dictionary<int, List<int>> graph, int start, int target)
            {
                if(!set.Contains(start))
                {
                    set.Add(start);
                    if (start == target) return true;
                    foreach (var item in graph[start])
                    {
                        if (Domoo(graph, item, target)) return true;
                    }
                }

                return false;
            }

            foreach (var e in edges)
            {
                var e1 = e[0];
                var e2 = e[1];
                set.Clear();

                if ((gg.ContainsKey(e1) && gg[e1].Any()) &&
                    (gg.ContainsKey(e2) && gg[e2].Any()) && 
                    Domoo(gg, e1, e2))
                    return e;

                if (!gg.ContainsKey(e1))
                    gg.Add(e1, new List<int>(){e2});
                else
                    gg[e1].Add(e2);

                if (!gg.ContainsKey(e2))
                    gg.Add(e2, new List<int>(){e1});
                else
                    gg[e2].Add(e1);
            }



            return res;

        }



    }
}