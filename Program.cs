﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using leetc;

namespace leetc1
{
    enum ProglemType
    {
        REGULAR,
        LINKED_LIST,
        TREE
    }
    class Program
    {
        static void Main(string[] args)
        {
            var input = ProglemType.REGULAR;
            // RegularMagic();
            // LinkedListMagic();
            // TreeMagic();

            switch (input)
            {
                case ProglemType.REGULAR: 
                    RegularMagic();
                    break;
                case ProglemType.LINKED_LIST:
                    LinkedListMagic();
                    break;
                case ProglemType.TREE:
                    TreeMagic();
                    break;
                default:
                    System.Console.WriteLine("not valid");
                    break;
            }

        }

        static void LinkedListMagic()
        {
            var ls = new LinkedListSolution();

            var input1 = ParseInputToListNode("[1,2,3,4,5]");
            var input2 = ParseInputToListNode("[9,9,9,9]");

            Console.WriteLine(
                ParseListNodeToSb(
                    ls.ReverseBetween(
                        input1, 1, 4
                        )
                )
            );



            ListNode ParseInputToListNode(string inp)
            {
                string inputTrimmed = inp.Substring(1, inp.Length - 2);
                var a = ",";
                string[] arr = inputTrimmed.Split(a);

                var res = new ListNode();
                var temp = res;

                foreach (var n in arr)
                {
                    temp.next = new ListNode(Int32.Parse(n));


                    temp = temp.next;
                }

                return res.next;
            }

            StringBuilder ParseListNodeToSb(ListNode ln)
            {
                var sb = new StringBuilder("[");
                while (ln != null)
                {
                    sb.Append(ln.val);

                    if (ln.next != null)
                    {
                        sb.Append(" ");
                    }

                    ln = ln.next;
                }
                sb.Append("]");
                return sb;
            }
        }

        static void TreeMagic()
        {
            
            var st = new TreeSolution();

            var tree = new TreeNode(1);
            tree.left = new TreeNode(3);
            tree.left.left = new TreeNode(5);
            // tree.left.right = new TreeNode(4);
            
            // tree.left.left.left = new TreeNode(1);

            tree.right = new TreeNode(2);
            // tree.right.right = new TreeNode(8);
            // tree.right.right.left = new TreeNode(7);
            // tree.right.right.right = new TreeNode(9);


            var tree2 = new TreeNode(2);
            tree2.left = new TreeNode(1);
            tree2.left.right = new TreeNode(4);
            tree2.right = new TreeNode(3);
            tree2.right.right = new TreeNode(7);
            
            var res = st.MergeTrees_BFS(tree, tree2);
        }

        static void RegularMagic()
        {
            var s = new Solution();

            var arr = new int[][]{
                new int[]{1,2,3},
                new int[]{4,5,6},
                new int[]{7,8,9},
                };




            var input1 = new int[] {3,2,4};
            //           new int[] {2,5,6 }, 
            // Console.WriteLine(ParseNestedList(
            //     s.Subsets(
            //         arr
            //         )));
            //[]


            //"aaaba aaaba aaba aaaba aaaba aaaba aaaba"
            //"aaaba"
            

            System.Console.WriteLine(
                ParseNestedList<string>(
                        s.GroupAnagrams2(
                           new string[]{
                               "ab", "ba", "a", "a", "c"
                           }
                        )));

            System.Console.WriteLine(s.Some());
            // arr.ToList().ForEach(x => 
            //         Console.WriteLine(
            //             $"arr: {ParseArray<int>(x)} --- ans: {ParseArray<int>(s.WordSubsets(x))}"
            //             )
            //      );

            // System.Console.WriteLine(s.Some(20));
        }

        static StringBuilder ParseArray<T>(T[] arr)
        {
            var sb = new StringBuilder("[");
            for (int i = 0; i < arr.Length; i++)
            {
                sb.Append(arr[i]);

                if (i != arr.Length - 1)
                {
                    sb.Append(" ");
                }
            }
            sb.Append("]");
            return sb;
        }

        static StringBuilder ParseList<T>(IList<T> arr)
        {
            var sb = new StringBuilder("[");
            for (int i = 0; i < arr.Count; i++)
            {
                sb.Append(arr[i]);

                if (i != arr.Count - 1)
                {
                    sb.Append(" ");
                }
            }
            sb.Append("]");
            return sb;
        }

        static StringBuilder ParseNestedList<T>(IList<IList<T>> arr)
        {
            var sb = new StringBuilder("[");

            for (int i = 0; i < arr.Count; i++)
            {
                sb.Append("[");

                for (int j = 0; j < arr[i].Count; j++)
                {
                    sb.Append(arr[i][j]);

                    if (j != arr[i].Count - 1)
                    {
                        sb.Append(" ");
                    }
                }
                sb.Append("],");
            }
            sb.Length--;
            sb.Append("]");
            return sb;
        }

        static int[][] TransformInput(string input)
        {
            var res = new List<int[]>() { };
            string inputTrimmed = input.Substring(1, input.Length - 3);

            var a = "],";
            string[] arr = inputTrimmed.Split(a);

            foreach (var item in arr)
            {
                var item1 = item.Substring(1);

                var itemTrimmed = item1.Split(',');

                int[] one = new int[] { Int32.Parse(itemTrimmed[0]), Int32.Parse(itemTrimmed[1]) };
                res.Add(one);
            }

            return res.ToArray();
        }

    }
}
