using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Security.Cryptography;
using System.Text;
using Microsoft.VisualBasic;

namespace leetc
{
    public class Solution
    {
        public int[] CountPoints(int[][] points, int[][] queries)
        {
            const int X_COOR_INDEX = 0;
            const int Y_COOR_INDEX = 1;
            const int RADIUS_INDEX = 2;

            bool ch_coor_in(int[] points, int[] queries)
            {
                var delta_x = queries[X_COOR_INDEX] - points[X_COOR_INDEX];
                var delta_y = queries[Y_COOR_INDEX] - points[Y_COOR_INDEX];

                var hip = Math.Sqrt((Math.Pow(delta_x, 2)) + (Math.Pow(delta_y, 2)));
                return hip <= queries[RADIUS_INDEX];
            }

            bool CheckMainFrame(int[] points, int[] queries, int index, int frame)
            {
                return queries[index] == points[index] &&
                queries[frame] + queries[RADIUS_INDEX] >= points[frame] &&
                queries[frame] - queries[RADIUS_INDEX] <= points[frame];
            }


            var res = new int[queries.Length];

            for (var i = 0; i < queries.Length; i++)
            {
                var count = 0;
                foreach (var point in points)
                {
                    if (CheckMainFrame(point, queries[i], X_COOR_INDEX, Y_COOR_INDEX) ||
                    CheckMainFrame(point, queries[i], Y_COOR_INDEX, X_COOR_INDEX))
                    {
                        count++;
                    }

                    else if (ch_coor_in(point, queries[i]))
                    {
                        count++;
                    }
                }
                res[i] = count;
            }

            return res;
        }

        public int[] MinOperations(string boxes)
        {
            var bl = boxes.Length;
            var res = new int[bl];

            var onesBehind = 0;
            var onesForward = 0;

            var steps = 0;

            for (int j = 0; j < bl; j++)
            {
                if (boxes[j] == '1')
                {
                    steps += j;
                    onesForward++;
                }
            }


            for (int i = 0; i < bl; i++)
            {
                if (boxes[i] == '1')
                {
                    onesBehind++;
                    onesForward--;
                }

                res[i] = steps;

                steps += onesBehind;
                steps -= onesForward;
            }

            return res;
        }

        public int MinCostToMoveChips(int[] position)
        {
            var minSteps = Int32.MaxValue;


            for (int j = 0; j < position.Length; j++)
            {
                var first = position[j];
                var steps = 0;

                foreach (var i in position)
                {
                    if (i == first) continue;

                    if ((Math.Abs(i - first) % 2) == 0) continue;

                    steps++;
                }

                minSteps = Math.Min(steps, minSteps);
            }
            return minSteps;
        }

        public IList<IList<int>> GroupThePeople1(int[] groupSizes)
        {
            var res = new List<IList<int>>() { };

            var groupSizes2 = new List<(int, int)>() { };

            for (int i = 0; i < groupSizes.Length; i++)
            {
                groupSizes2.Add((groupSizes[i], i));
            }

            groupSizes2.Sort();

            var it = 0;
            while (it != groupSizes.Length)
            {
                var firstP = groupSizes2[it];
                var resArr = groupSizes2.Skip(it).Take(firstP.Item1).ToList().Select((x) => x.Item2).ToList();

                res.Add(resArr);

                it += firstP.Item1;
            }

            return res;
        }

        public IList<IList<int>> GroupThePeople(int[] groupSizes)
        {
            var res = new List<IList<int>>() { };

            var groupSizes3 = new Dictionary<int, List<int>>();

            for (int i = 0; i < groupSizes.Length; i++)
            {
                if (!groupSizes3.Keys.Contains(groupSizes[i]))
                {
                    groupSizes3.Add(groupSizes[i], new List<int>() { });

                }

                groupSizes3[groupSizes[i]].Add(i);

                if (groupSizes3[groupSizes[i]].Count == groupSizes[i])
                {
                    res.Add(groupSizes3[groupSizes[i]]);
                    groupSizes3.Remove(groupSizes[i]);
                }

            }

            return res;
        }

        public int MaxIncreaseKeepingSkyline(int[][] grid)
        {
            var ans = 0;
            var maxForCols = new int[grid[0].Length];
            var maxForRows = new int[grid.Length];

            for (int i = 0; i < grid.Length; i++)
            {
                for (int j = 0; j < grid[i].Length; j++)
                {
                    maxForRows[i] = Math.Max(maxForRows[i], grid[i][j]);
                    maxForCols[j] = Math.Max(maxForCols[j], grid[i][j]);
                }
            }

            for (int i = 0; i < grid.Length; i++)
            {
                for (int j = 0; j < grid[i].Length; j++)
                {
                    ans += Math.Min(maxForRows[i], maxForCols[j]) - grid[i][j];
                }
            }

            return ans;
        }

        public string SmallestSubsequence(string s)
        {
            var checkList = new HashSet<char>() { };
            var stack = new Stack<char>() { };
            var occurencies = new int[26];

            foreach (var c in s)
            {
                occurencies[c - 'a']++;
            }



            foreach (var c in s)
            {
                occurencies[c - 'a']--;

                if (!checkList.Contains(c))
                {
                    checkList.Add(c);

                    while (stack.Any() &&
                        stack.Peek() > c &&
                        occurencies[stack.Peek() - 'a'] > 0)
                    {
                        char toRemove = stack.Pop();
                        checkList.Remove(toRemove);
                    }

                    stack.Push(c);
                }
            }

            var res = new StringBuilder();
            var temp = new StringBuilder();

            while (stack.Any())
            {
                temp.Append(stack.Pop());

                temp.Append(res);
                res.Clear();
                res.Append(temp);
                temp.Clear();
            }


            return res.ToString();
        }

        public int Trap(int[] height)
        {
            var ans = 0;

            var leftTrap = 0;
            var rightTrap = 0;
            var seqToFil = new List<int>() { };

            int FillWatter(List<int> seq, int upTo)
            {
                int res = 0;

                for (int i = 0; i < seq.Count(); i++)
                {
                    if (seq[i] < upTo)
                    {
                        res += upTo - seq[i];
                        seq[i] = upTo;
                    }
                }


                return res;
            }

            for (int j = 0; j < height.Length; j++)
            {
                var i = height[j];

                if (i >= leftTrap && leftTrap > 0 && seqToFil.Any() ||

                    j == height.Length - 1)
                {
                    rightTrap = Math.Max(rightTrap, i);
                    ans += FillWatter(seqToFil, Math.Min(leftTrap, rightTrap));
                    leftTrap = i;
                    rightTrap = 0;
                    seqToFil.Clear();
                    continue;
                }

                if (i > leftTrap)
                {
                    leftTrap = i;
                    continue;
                }

                if (i < leftTrap)
                {
                    if (i < rightTrap)
                    {
                        ans += FillWatter(seqToFil, Math.Min(leftTrap, rightTrap));
                    }
                    rightTrap = i;

                    seqToFil.Add(i);
                }


            }

            return ans;
        }

        public IList<IList<int>> Subsets_1(int[] nums)
        {
            var res = new HashSet<IList<int>>() { };

            var n = nums.Length;

            void SoomeM(List<int> sub, int[] arr)
            {
                if (arr.Length == 0)
                {
                    res.Add(sub);
                    return;
                }
                var sub1 = new List<int>() { };
                sub1.AddRange(sub);
                SoomeM(sub1, arr[1..]);

                var sub2 = new List<int>() { };
                sub2.AddRange(sub);
                sub2.Add(arr[0]);
                SoomeM(sub2, arr[1..]);

            }

            SoomeM(new List<int>() { }, nums);

            return res.ToList();
        }

        public IList<IList<int>> Subsets(int[] nums)
        {
            var res = new List<IList<int>>() { };

            void SoomeM(int index, List<int> sub, int[] arr)
            {
                if (index == arr.Length)
                {
                    return;
                }

                for (int i = index; i < arr.Length; i++)
                {

                    sub.Add(arr[i]);
                    res.Add(new List<int>(sub));
                    SoomeM(i + 1, sub, nums);
                    sub.RemoveAt(sub.Count() - 1);
                }
            }

            SoomeM(0, new List<int>() { }, nums);

            return res.ToList();
        }

        public IList<IList<int>> SubsetsWithDup1(int[] nums)
        {
            var compa = new Compa();
            var res = new List<IList<int>>() { new List<int>() { } };

            void DoSubset(int index, List<int> sub)
            {
                if (index == nums.Length)
                {
                    return;
                }

                for (int i = index; i < nums.Length; i++)
                {
                    sub.Add(nums[i]);
                    if (!res.Contains(sub, compa))
                        res.Add(new List<int>(sub));
                    DoSubset(i + 1, sub);
                    sub.RemoveAt(sub.Count() - 1);
                }
            }

            DoSubset(0, new List<int>() { });


            return res.ToList();
        }

        class Compa : IEqualityComparer<IList<int>>
        {
            public bool Equals(IList<int>? x, IList<int>? y)
            {
                if (x.Count() != y.Count()) { return false; }

                var first = x.GroupBy(x => x)
                    .ToDictionary(y => y.Key, y => y.Count());
                var second = y.GroupBy(x => x)
                    .ToDictionary(y => y.Key, y => y.Count());

                var b = first.OrderBy(x => x.Key).SequenceEqual(second.OrderBy(y => y.Key));

                return b;
            }

            public int GetHashCode(IList<int> obj)
            {
                unchecked
                {
                    int hash = 19;
                    foreach (var foo in obj)
                    {
                        hash = hash * 31 + foo.GetHashCode();
                    }
                    return hash;
                }
            }
        }


        public IList<IList<int>> SubsetsWithDup(int[] nums)
        {
            Array.Sort(nums);
            var res = new List<IList<int>>() { new List<int>() { } };

            void DoSubset(int index, List<int> sub, int[] nums)
            {
                if (index == nums.Length)
                {
                    return;
                }

                for (int i = index; i < nums.Length; i++)
                {
                    if (index != i && nums[i] == nums[i - 1])
                    {
                        continue;
                    }

                    sub.Add(nums[i]);
                    res.Add(new List<int>(sub));
                    DoSubset(i + 1, sub, nums);
                    sub.RemoveAt(sub.Count() - 1);
                }
            }

            DoSubset(0, new List<int>() { }, nums);


            return res.ToList();
        }

        public int CountLargestGroup(int n)
        {
            var dd = new Dictionary<int, int>() { };
            var currentMax = 0;
            var countCurrentMax = 0;

            int CalcSumNum(int n)
            {
                var sum = 0;
                while (n != 0)
                {
                    var temp = n % 10;
                    sum += temp;
                    n /= 10;

                }

                return sum;
            }

            for (int i = 1; i <= n; i++)
            {
                var s = CalcSumNum(i);
                if (!dd.ContainsKey(s))
                {
                    dd.Add(s, 0);
                }
                dd[s]++;

                if (dd[s] == currentMax)
                {
                    countCurrentMax++;
                }
                if (dd[s] > currentMax)
                {
                    currentMax = dd[s];
                    countCurrentMax = 1;
                }
            }


            return countCurrentMax;
        }

        public int LargestAltitude(int[] gain)
        {
            int res = 0;

            int currentAlt = 0;

            foreach (var i in gain)
            {
                currentAlt += i;

                res = Math.Max(res, currentAlt);
            }

            return res;
        }

        public int MaximumPopulation(int[][] logs)
        {
            var res = logs[0][0];
            var maxCount = 0;

            foreach (var years in logs)
            {
                var startYear = years[0];
                var endYear = years[1];
                var count = 0;

                foreach (var yAgain in logs)
                {
                    var startY = yAgain[0];
                    var endY = yAgain[1];

                    if (endY > startYear && startY <= startYear)
                    {
                        count++;
                    }
                }

                if (count > maxCount || (count == maxCount && res > startYear))
                {
                    maxCount = count;
                    res = startYear;
                }
            }

            return res;
        }

        public int SubsetXORSum1(int[] nums)
        {
            var res = 0;

            void Exchange(int index, List<int> sub)
            {
                if (index == nums.Length)
                {
                    res += sub.Aggregate(0, (x, y) => x ^ y);
                    return;
                }

                var sub1 = new List<int>();
                sub1.AddRange(sub);
                Exchange(index + 1, sub1);

                var sub2 = new List<int>();
                sub2.Add(nums[index]);
                sub2.AddRange(sub);
                Exchange(index + 1, sub2);
            }

            Exchange(0, new List<int>());

            return res;
        }

        public int SubsetXORSum(int[] nums)
        {
            var res = 0;

            void Exchange(int index, List<int> sub)
            {
                if (index == nums.Length)
                {
                    return;
                }


                for (int i = index; i < nums.Length; i++)
                {
                    sub.Add(nums[i]);
                    res += sub.Aggregate(0, (x, y) => x ^ y);
                    Exchange(i + 1, sub);
                    sub.RemoveAt(sub.Count() - 1);

                }

            }

            Exchange(0, new List<int>());

            return res;
        }


        public int[] DailyTemperatures1(int[] temperatures)
        {
            var res = new List<int>() { };
            var dd = new SortedDictionary<int, int>() { };

            bool IsInDict(int c, int index)
            {
                if (!dd.Any()) return false;
                if (dd.Last().Key <= c) return false;

                var next = new KeyValuePair<int, int>(0, 0);
                foreach (var kp in dd)
                {
                    if (kp.Key > c && (kp.Value < next.Value || next.Value == 0))
                    {
                        next = kp;
                    }


                }

                res.Add(next.Value - index);
                return true;
            }

            // for (int i = 0; i < temperatures.Length; i++)
            // {

            // }

            for (int i = 0; i < temperatures.Length; i++)
            {
                var current = temperatures[i];
                var countDays = 0;
                bool added = false;

                if (dd.ContainsKey(current)) dd.Remove(current);



                for (int jj = i + 1; jj < temperatures.Length; jj++)
                {
                    countDays++;
                    if (jj == i + 1 && temperatures[jj] > current)
                    {
                        res.Add(1);
                        added = true;
                        break;
                    }

                    if (IsInDict(current, i))
                    {
                        added = true;
                        break;
                    }

                    if (!dd.ContainsKey(temperatures[jj]))
                    {
                        dd.Add(temperatures[jj], jj);
                    }

                    if (temperatures[jj] > current)
                    {
                        res.Add(countDays);
                        added = true;
                        break;
                    }
                }

                if (!added) res.Add(0);
            }

            return res.ToArray();
        }

        public int[] DailyTemperatures(int[] temperatures)
        {
            var res = new int[temperatures.Length];
            var stack = new Stack<(int, int)>();

            for (int i = 0; i < temperatures.Length; i++)
            {
                var current = temperatures[i];


                while (stack.Any() && stack.Peek().Item1 < current)
                {
                    var (prev, prevIndex) = stack.Pop();

                    res[prevIndex] = i - prevIndex;
                }

                stack.Push((current, i));


            }
            res[res.Length - 1] = 0;
            return res;
        }

        public int[] NextGreaterElement1(int[] nums1, int[] nums2)
        {
            var res = new int[nums1.Length];

            for (int i = 0; i < nums1.Length; i++)
            {
                var current = nums1[i];
                var nextElement = 0;
                var found = false;
                for (int jj = 0; jj < nums2.Length; jj++)
                {
                    if (nums2[jj] != current && !found) continue;
                    else found = true;

                    if (nums2[jj] > current && found)
                    {
                        nextElement = nums2[jj];
                        break;
                    }
                }
                res[i] = nextElement != 0 ? nextElement : -1;
            }

            return res;
        }

        public int[] NextGreaterElement(int[] nums1, int[] nums2)
        {
            var res = new int[nums1.Length];
            var stack = new Stack<int>();
            var dd = new Dictionary<int, int>();

            for (int i = 0; i < nums2.Length; i++)
            {
                var current = nums2[i];

                while (stack.Any() && stack.Peek() < current)
                {
                    dd.Add(stack.Pop(), current);
                }

                stack.Push(current);
            }

            for (int i = 0; i < nums1.Length; i++)
            {
                res[i] = dd.ContainsKey(nums1[i]) ? dd[nums1[i]] : -1;
            }

            return res;
        }

        public int[] NextGreaterElements1(int[] nums)
        {

            var extendedList = new List<int>(nums) { };
            extendedList.AddRange(nums[..(nums.Length)]);

            var res = new int[nums.Length];

            for (int i = 0; i < nums.Length; i++)
            {
                var current = nums[i];
                var found = false;
                var nextElement = 0;

                for (int jj = 0; jj < nums.Length; jj++)
                {
                    if (extendedList[jj + i + 1] > current)
                    {
                        nextElement = extendedList[jj + 1 + i];
                        found = true;
                        break;
                    };
                }
                res[i] = found ? nextElement : -1;

            }

            return res;
        }

        public int[] NextGreaterElements(int[] nums)
        {
            var size = nums.Length;
            var res = new int[size];
            var stack = new Stack<int>();


            for (int i = 0; i < size * 2; i++)
            {
                var index = i % size;

                while (stack.Any() && nums[stack.Peek()] < nums[index])
                {
                    res[stack.Pop()] = nums[index];
                }

                if (i < size) stack.Push(index);
            }

            while (stack.Any())
            {
                res[stack.Pop()] = -1;
            }


            return res;
        }

        public IList<string> WordSubsets(string[] words1, string[] words2)
        {
            var res = new List<string>() { };

            var charSet = new int[26];

            foreach (var sub in words2)
            {
                var alphabet = new int[26];
                foreach (var c in sub)
                {
                    alphabet[c - 97]++;
                    if (alphabet[c - 97] > charSet[c - 97])
                    {
                        charSet[c - 97]++;
                    }
                }
            }

            foreach (var word in words1)
            {
                var all = true;

                var alphabet = new int[26];
                foreach (var ch in word)
                {
                    alphabet[ch - 97]++;
                }

                for (int i = 0; i < 26; i++)
                {
                    if (charSet[i] > alphabet[i])
                    {
                        all = false;
                        break;
                    }
                }

                if (all)
                {
                    res.Add(word);
                }
            }

            return res;
        }

        public bool IsSubsequence(string s, string t)
        {
            var res = false;

            var j = 0;
            for (int i = 0; i < t.Length; i++)
            {

                if (j < s.Length && t[i] == s[j]) j++;
                if (j == s.Length) res = true;
            }
            if (j == s.Length) res = true;
            return res;
        }

        public int NumMatchingSubseq1(string s, string[] words)
        {

            var res = 0;
            var set = new HashSet<string>();
            var setNot = new HashSet<string>();
            bool IsSubsequence(string s, string t)
            {
                var res = false;

                var j = 0;
                for (int i = 0; i < t.Length; i++)
                {

                    if (j < s.Length && t[i] == s[j]) j++;
                    if (j == s.Length) res = true;
                }

                return res;
            }

            foreach (var word in words)
            {
                if (set.Contains(word)) res++;
                else if (setNot.Contains(word)) continue;

                else if (IsSubsequence(word, s))
                {
                    res++;
                    set.Add(word);
                }
                else setNot.Add(word);

            }

            return res;
        }

        public int NumMatchingSubseq2(string s, string[] words)
        {

            var res = 0;
            var set = new HashSet<string>();
            var setNot = new HashSet<string>();

            foreach (var word in words)
            {
                if (!setNot.Contains(word))
                {
                    if (set.Contains(word)) res++;
                    else
                    {
                        var res1 = false;

                        var j = 0;
                        for (int i = 0; i < s.Length; i++)
                        {

                            if (j < word.Length && s[i] == word[j]) j++;
                            if (j == word.Length)
                            {
                                res1 = true;
                                break;
                            }
                        }

                        if (res1)
                        {
                            set.Add(word);
                            res++;
                        }
                        else
                        {
                            setNot.Add(word);
                        }
                    }
                }
            }

            return res;
        }

        public int NumMatchingSubseq3(string s, string[] words)
        {

            var res = 0;
            var set = new HashSet<string>();
            var setNot = new HashSet<string>();

            foreach (var word in words)
            {
                if (set.Contains(word)) res++;
                else if (setNot.Contains(word)) continue;

                else
                {
                    var res1 = false;

                    var j = 0;
                    for (int i = 0; i < s.Length; i++)
                    {

                        if (j < word.Length && s[i] == word[j]) j++;
                        if (j == word.Length)
                        {
                            res1 = true;
                            break;
                        }
                    }

                    if (res1)
                    {
                        set.Add(word);
                        res++;
                    }
                    else
                    {
                        setNot.Add(word);
                    }
                }

            }

            return res;
        }

        public int NumMatchingSubseq(string s, string[] words)
        {
            var record = new List<StringBuilder>[26];
            for (int i = 0; i < 26; i++)
                record[i] = new List<StringBuilder>();

            int res = 0;

            foreach (var word in words)
                record[word[0] - 'a'].Add(new StringBuilder(word));

            foreach (var l in s)
            {
                var temp = record[l - 'a'];
                record[l - 'a'] = new List<StringBuilder>();
                foreach (var word in temp)
                {
                    word.Remove(0, 1);
                    if (word.Length == 0)
                        res++;
                    else
                        record[word[0] - 'a'].Add(word);
                }
            }

            return res;
        }

        public string LongestNiceSubstring(string s)
        {
            var res = new StringBuilder("");
            var found = new HashSet<string>();
            var notFound = new HashSet<string>();

            void CheckChar(char c1)
            {
                var c = c1.ToString();

                if (found.Contains(c, StringComparer.OrdinalIgnoreCase)) return;

                if (!notFound.Contains(c.ToLower()) || !notFound.Contains(c.ToUpper()))
                {
                    notFound.Add(c);
                }

                if (notFound.Contains(c.ToLower()) && notFound.Contains(c.ToUpper()))
                {
                    notFound.Remove(c.ToLower());
                    notFound.Remove(c.ToUpper());
                    found.Add(c);
                }
            }

            for (int i = 0; i < s.Length; i++)
            {
                if (res.Length != 0 && s.Length - i <= res.Length) break;

                var tempRes = new StringBuilder(s[i].ToString());

                found.Clear();
                notFound.Clear();

                CheckChar(s[i]);

                for (int jj = i + 1; jj < s.Length; jj++)
                {

                    tempRes.Append(s[jj]);
                    CheckChar(s[jj]);

                    if (!notFound.Any() && tempRes.Length > res.Length)
                    {
                        res.Clear();
                        res.Append(tempRes);

                    }

                }
            }

            return res.ToString();
        }

        public string LongestNiceSubstring2(string s)
        {
            var all = new HashSet<char>();


            foreach (var c in s)
            {
                all.Add(c);
            }

            for (int i = 0; i < s.Length; i++)
            {
                var cc = s[i];
                if (all.Contains(char.ToLower(cc)) &&
                    all.Contains(char.ToUpper(cc))) continue;

                var s1 = LongestNiceSubstring2(s.Substring(0, i));
                var s2 = LongestNiceSubstring2(s.Substring(i + 1));

                return s1.Length >= s2.Length ? s1 : s2;
            }
            return s;
        }


        public int CalPoints(string[] ops)
        {
            var stack = new Stack<int>();
            var res = 0;

            foreach (var item in ops)
            {
                switch (item)
                {
                    case "D":
                        res += stack.Peek() * 2;
                        stack.Push(stack.Peek() * 2);
                        break;
                    case "C":
                        res -= stack.Pop();
                        break;
                    case "+":
                        var t = stack.Pop();
                        var s = t + stack.Peek();
                        stack.Push(t);
                        stack.Push(s);
                        res += stack.Peek();
                        break;

                    default:
                        stack.Push(Int32.Parse(item));
                        res += stack.Peek();
                        break;
                }
            }

            return res;
        }

        public string RemoveDuplicates(string s)
        {
            var stack = new Stack<char>();

            foreach (var c in s)
            {
                if (stack.Count > 0 && stack.Peek() == c)
                {
                    stack.Pop();
                }
                else
                {
                    stack.Push(c);
                }
            }

            var res = new StringBuilder(stack.Count);
            var arr = stack.ToArray();

            for (int i = arr.Length - 1; i >= 0; i--)
            {
                res.Append(arr[i]);
            }

            return res.ToString();
        }

        public string MakeGood(string s)
        {
            var stack = new Stack<char>();
            var res = new StringBuilder();

            for (int i = s.Length - 1; i >= 0; i--)
            {
                var c = s[i];

                if (stack.Any())
                {
                    if (char.IsUpper(c) && char.ToLower(c) == stack.Peek())
                    {
                        stack.Pop();
                        continue;
                    }
                    if (char.IsLower(c) && char.ToUpper(c) == stack.Peek())
                    {
                        stack.Pop();
                        continue;
                    }
                    stack.Push(c);
                    continue;
                }
                stack.Push(c);

            }

            while (stack.Any())
            {
                res.Append(stack.Pop());
            }



            return res.ToString();
        }

        public IList<string> BuildArray(int[] target, int n)
        {
            var res = new List<string>();
            var push = "Push";
            var pop = "Pop";

            var jj = 0;

            // for (int i = 1; i <= n; i++)
            // {
            //     res.Add(push);
            //     if (target[jj] != i)
            //     {
            //         res.Add(pop);
            //         jj--;
            //     }

            //     jj++;
            //     if(jj == target.Length) break;
            // }

            var i = 1;
            var n1 = target.Length;
            while (jj != n1 && i <= n)
            {
                res.Add(push);
                if (target[jj] != i)
                {
                    res.Add(pop);
                    jj--;
                }
                jj++;
                i++;
            }


            return res;
        }


        public string RemoveOuterParentheses(string s)
        {
            var res = new StringBuilder();
            var open = 0;

            foreach (var c in s)
            {
                if (c == '(')
                {
                    open++;
                    if (open > 1)
                    {
                        res.Append(c);
                    }
                }
                else
                {
                    open--;
                    if (open > 0)
                    {
                        res.Append(c);
                    }
                }
            }

            return res.ToString();
        }

        public int ShipWithinDays1(int[] weights, int days)
        {
            var sum = weights.Sum();
            if (days == 1) return sum;

            var res = weights.Max();
            var tempCargo = 0;
            var daysRemaining = days;


            var i = 0;
            while (true)
            {
                if (daysRemaining >= 1 && i == weights.Length || daysRemaining == 0)
                {
                    if (tempCargo <= res && i == weights.Length) break;
                    else
                    {
                        daysRemaining = days;
                        i = 0;
                        tempCargo = 0;
                        res++;
                    }
                }
                var w = weights[i];
                tempCargo += w;

                if (tempCargo == res)
                {
                    daysRemaining--;
                    tempCargo = 0;
                }
                if (tempCargo > res)
                {
                    tempCargo = 0;
                    daysRemaining--;
                    continue;

                }

                i++;
            }

            return res;
        }

        public int ShipWithinDays(int[] weights, int days)
        {
            if (days == 1) return weights.Sum();

            var max = 50000;
            var min = 1;
            var tempCargo = 0;
            var daysRemaining = days;
            var capacity = min;

            while (min < max)
            {
                capacity = (max + min) / 2;
                var i = 0;
                tempCargo = 0;
                daysRemaining = days;

                while (i < weights.Length && daysRemaining > 0)
                {
                    tempCargo += weights[i];

                    if (tempCargo == capacity)
                    {
                        daysRemaining--;
                        tempCargo = 0;
                    }
                    if (tempCargo > capacity)
                    {
                        tempCargo = 0;
                        daysRemaining--;
                        continue;

                    }

                    i++;
                }
                if (i == weights.Length) max = capacity;
                else min = capacity + 1;

            }

            return min;
        }

        public int MinDays(int[] bloomDay, int m, int k)
        {
            if (m * k > bloomDay.Length) return -1;

            var res = -1;


            int DoMagic(int startIndex, int b)
            {
                if (startIndex > bloomDay.Length - k * m)
                {
                    return -1;
                }

                var min = -1;
                for (int i = startIndex; i < bloomDay.Length - k; i += k)
                {
                    var currentMin = bloomDay[i..(i + k - 1)].Max();
                    min = Math.Max(min, currentMin);
                }

                return min;
            }


            for (int i = 0; i < k; i++)
            {
                var currentMin = bloomDay[i..(i + k - 1)].Max();


            }



            return res;
        }


        public void Merge(int[] nums1, int m, int[] nums2, int n)
        {
            if (n == 0) return;
            if (m == 0)
            {
                for (int i = 0; i < nums2.Length; i++)
                {
                    nums1[i] = nums2[i];
                }
            }

            var buffer = new Queue<int>();

            var i_m = 0;
            var j_n = 0;
            while (i_m < m + n || j_n < n)
            {
                var first = buffer.Any() ? buffer.Peek() : nums1[i_m];

                if (!buffer.Any() && i_m >= m)
                {
                    nums1[i_m] = nums2[j_n];
                    j_n++;
                    i_m++;
                    continue;
                }

                if (j_n < n && nums2[j_n] <= first)
                {
                    if (i_m < m) buffer.Enqueue(nums1[i_m]);
                    nums1[i_m] = nums2[j_n];
                    j_n++;
                    i_m++;
                    continue;
                }


                if (buffer.Any())
                {
                    if (first <= nums1[i_m])
                    {
                        if (i_m < m) buffer.Enqueue(nums1[i_m]);
                        nums1[i_m] = buffer.Dequeue();
                    }

                    else if (first > nums1[i_m])
                    {
                        nums1[i_m] = buffer.Dequeue();
                    }


                }

                i_m++;

            }

        }

        public int MaxRepeating(string sequence, string word)
        {
            var maxOc = sequence.Length / word.Length;
            var i = 0;
            var res = 0;

            var currentWord = word;
            for (int j = 1; j <= maxOc; j++)
            {

                i = sequence.IndexOf(currentWord);

                if (i == -1) break;

                res++;
                currentWord += word;
            }

            return res;
        }

        public void SortColors(int[] nums)
        {
            var zeros = 0;
            var twos = nums.Length - 1;
            var current = 0;

            while (current <= twos)
            {
                if (nums[current] == 0)
                {
                    if (current != zeros)
                    {
                        nums[current] = nums[zeros];
                        nums[zeros] = 0;
                    }

                    zeros++;
                    current++;
                }
                else if (nums[current] == 2)
                {
                    while (nums[twos] == 2 && twos > current)
                    {
                        twos--;
                    }
                    nums[current] = nums[twos];
                    nums[twos] = 2;
                    twos--;
                }
                else current++;
            }
        }

        public IList<IList<int>> ThreeSum(int[] nums)
        {
            Array.Sort(nums);
            var res = new List<IList<int>>();

            if (nums.Length < 3) return res;

            for (int i = 0; i < nums.Length - 2; i++)
            {
                if (i != 0 && nums[i] == nums[i - 1]) continue;

                var idx1 = i + 1;
                var idx2 = nums.Length - 1;

                while (idx1 < idx2)
                {
                    var sum = nums[i] + nums[idx1] + nums[idx2];
                    if (sum == 0)
                    {
                        res.Add(new List<int>() { nums[i], nums[idx1], nums[idx2] });
                        idx1++;
                        while (idx1 < idx2 && nums[idx1] == nums[idx1 - 1]) idx1++;
                    }

                    if (sum > 0) idx2--;
                    else if (sum < 0) idx1++;
                }

            }

            return res;
        }


        public int[] Intersect(int[] nums1, int[] nums2)
        {
            var res = new List<int>();
            var dd = new Dictionary<int, int>();
            foreach (var i in nums1)
            {
                if (!dd.ContainsKey(i)) dd.Add(i, 1);
                else dd[i]++;
            }

            foreach (var i in nums2)
            {
                if (dd.ContainsKey(i))
                {
                    dd[i]--;
                    if (dd[i] < 0)
                    {
                        dd.Remove(i);
                    }
                    else res.Add(i);
                }
            }

            return res.ToArray();
        }

        public int MaxArea(int[] height)
        {
            var res = 0;
            var current = 0;

            var start = 0;
            var end = height.Length - 1;

            for (int i = 0; i < height.Length; i++)
            {
                if (height[i] == 0 || height[i] <= start) continue;


                var currentMaxEnd = height[end];

                while (end > i)
                {
                    if (height[end] < currentMaxEnd || height[end] == 0)
                    {
                        end--;
                        continue;
                    }

                    currentMaxEnd = height[end];
                    var len = end - i;
                    var h = Math.Min(height[i], currentMaxEnd);
                    current = len * h;
                    if (current > res)
                    {
                        res = current;
                        start = height[i];
                    }
                    end--;

                }

                end = height.Length - 1;


            }


            return res;

        }

        public int MaxArea2(int[] height)
        {
            var res = 0;
            var start = 0;
            var end = height.Length - 1;

            while (end > start)
            {
                var a = Math.Min(height[start], height[end]) * (end - start);
                res = Math.Max(a, res);

                if (height[start] < height[end]) start++;
                else end--;
            }

            return res;
        }

        public int LengthOfLongestSubstring(string s)
        {
            var set = new HashSet<char>();
            var res = 0;

            var i = 0;
            while (i < s.Length)
            {
                var next = s[i];
                if (!set.Contains(next))
                {
                    set.Add(next);
                    res = Math.Max(res, set.Count);
                    i++;
                }
                else
                {
                    set.Remove(s[i - set.Count]);
                }
            }

            return res;
        }

        public IList<IList<int>> Generate(int numRows)
        {
            var res = new List<IList<int>>();
            res.Add(new List<int>() { 1 });
            if (numRows == 1) return res;

            var prev = new List<int>() { 1, 1 };
            res.Add(prev);
            if (numRows == 2) return res;

            for (int i = 2; i < numRows; i++)
            {
                var next = new List<int>() { 1 };
                for (int j = 1; j < i; j++)
                {
                    next.Add(prev[j - 1] + prev[j]);
                }
                next.Add(1);
                prev = next;
                res.Add(next);
            }



            return res;
        }


        public int[][] Merge1(int[][] intervals)
        {
            var res = new List<int[]>();
            Comparer<int> comparer = Comparer<int>.Default;
            Array.Sort(intervals, (x, y) => comparer.Compare(x[0], y[0]));
            var currentStart = intervals[0][0];
            var currentEnd = intervals[0][1];

            var i = 1;
            while (i <= intervals.Length - 1)
            {
                var nextStart = intervals[i][0];
                var nextEnd = intervals[i][1];

                if (nextStart >= currentStart && nextEnd <= currentEnd)
                {
                    i++;
                    continue;
                }

                if (nextStart >= currentStart &&
                    nextStart <= currentEnd && nextEnd > currentEnd)
                {
                    currentEnd = nextEnd;
                }
                else if (nextStart < currentStart && nextEnd >= currentStart &&
                        nextEnd <= currentEnd)
                {
                    currentStart = nextStart;
                }
                else
                {
                    res.Add(new int[] { currentStart, currentEnd });
                    currentStart = nextStart;
                    currentEnd = nextEnd;
                }

                i++;
            }

            res.Add(new int[] { currentStart, currentEnd });

            return res.ToArray();
        }

        public IList<IList<int>> Permute(int[] nums)
        {
            var res = new List<IList<int>>();
            var ceckSet = new HashSet<int>();

            void Domoo(List<int> r, int nextIndex)
            {
                if (nextIndex == nums.Length) res.Add(new List<int>(r));

                for (int i = 0; i < nums.Length; i++)
                {
                    if (ceckSet.Contains(nums[i])) continue;

                    r.Insert(nextIndex, nums[i]);
                    ceckSet.Add(nums[i]);

                    Domoo(r, nextIndex + 1);

                    r.RemoveAt(nextIndex);
                    ceckSet.Remove(nums[i]);
                }
            }

            Domoo(new List<int>(), 0);

            return res;
        }

        public IList<IList<int>> Permute1(int[] nums)
        {
            var res = new List<IList<int>>();

            void Domoo(List<int> r, int nextIndex, List<int> remaining)
            {
                if (nextIndex == nums.Length) res.Add(new List<int>(r));

                for (int i = 0; i < remaining.Count; i++)
                {
                    var t = remaining[i];
                    r.Insert(nextIndex, t);
                    remaining.RemoveAt(i);

                    Domoo(r, nextIndex + 1, new List<int>(remaining));

                    r.RemoveAt(nextIndex);
                    remaining.Insert(i, t);
                }
            }

            Domoo(new List<int>(), 0, nums.ToList());

            return res;
        }

        public string LongestCommonPrefix(string[] strs)
        {
            if (strs.Length == 1) return strs[0];

            var res = new StringBuilder();

            var testDummy = strs[strs.Length - 1];
            strs = strs.SkipLast(1).ToArray();

            var bCheck = true;
            for (int i = 0; i < testDummy.Length; i++)
            {

                var currentC = testDummy[i];

                foreach (var item in strs)
                {
                    if (item.Length <= i || item[i] != currentC)
                    {
                        bCheck = false;
                        break;
                    }
                }

                if (bCheck == false) return res.ToString();

                res.Append(currentC);
            }


            return res.ToString();
        }

        public bool IsPalindrome(int x)
        {
            if (x < 0) return false;
            if (x % 10 == x) return true;

            var ss = x.ToString();

            var start = 0;
            var end = ss.Length - 1;

            while (start < end)
            {
                if (ss[start] != ss[end]) return false;

                start++;
                end--;
            }

            return true;
        }

        public int ThirdMax(int[] nums)
        {
            var set = new HashSet<int>(nums);
            if (set.Count < 3) return set.Max();

            var a = set.ToArray();
            Array.Sort(a);

            return a[a.Length - 3];
        }

        public bool BuddyStrings(string s, string goal)
        {
            if (s.Length != goal.Length ||
                s.Length == 1 && goal.Length == 1) return false;

            if (s == goal)
            {
                for (int i = 0; i < s.Length - 1; i++)
                {
                    for (int k = i + 1; k < s.Length; k++)
                    {
                        if (s[i] == s[k]) return true;
                    }
                }
            }

            var idxArr = new List<int>();


            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] != goal[i])
                {
                    if (idxArr.Count == 2) return false;
                    idxArr.Add(i);
                }
            }

            if (idxArr.Count < 2) return false;


            if (s[idxArr[0]] == goal[idxArr[1]] &&
                s[idxArr[1]] == goal[idxArr[0]]) return true;


            return false;

        }

        public bool CanPlaceFlowers(int[] flowerbed, int n)
        {
            if (n == 0) return true;

            if (flowerbed.Length < 3 &&
                flowerbed.All(x => x == 0) &&
                n == 1) return true;

            if (flowerbed[0] == 0 &&
                flowerbed[1] == 0)
            {
                n--;
                flowerbed[0] = 1;
            }

            if (flowerbed[flowerbed.Length - 1] == 0 &&
                flowerbed[flowerbed.Length - 2] == 0)
            {
                n--;
                flowerbed[flowerbed.Length - 1] = 1;
            }

            int i = 1;
            while (i < flowerbed.Length - 1 && n > 0)
            {
                if (flowerbed[i] == 0 &&
                    flowerbed[i + 1] == 0 &&
                    flowerbed[i - 1] == 0)
                {
                    n--;
                    i += 2;
                    continue;
                }

                if (flowerbed[i + 1] == 1)
                {
                    i += 3;
                    continue;
                }

                if (flowerbed[i] == 1)
                {
                    i += 2;
                    continue;
                }

                if (flowerbed[i - 1] == 1)
                {
                    i++;
                    continue;
                }
            }

            return n <= 0;
        }

        public int NumSquares(int n)
        {
            var squares = new List<int>();

            var k = 1;
            var kk = 1;
            while (kk <= n)
            {
                squares.Add(kk);
                k++;
                kk = k * k;
            }
            squares.Reverse();
            var sum = Int32.MaxValue;


            void Domoo(int numb, int index, int s)
            {
                if (numb < 0 || s >= sum) return;
                if (numb == 0)
                {
                    sum = Math.Min(sum, s);
                    return;
                }

                while (index < squares.Count)
                {
                    var t = numb - squares[index];

                    Domoo(t, index, s + 1);
                    index++;

                }


            }

            Domoo(n, 0, 0);

            return sum;
        }

        public int NumSquares2(int n)
        {
            var dd = new int[n + 1];

            for (int i = 1; i <= n; i++)
            {
                dd[i] = int.MaxValue;

                for (int j = 1; j * j <= i; j++)
                {
                    dd[i] = Math.Min(dd[i], dd[i - j * j] + 1);
                }
            }

            return dd[n];
        }

        public void Rotate(int[][] matrix)
        {
            var end = matrix.Length - 1;

            var cycleEndR = matrix.Length / 2;
            var cycleEndC = (matrix[0].Length + 1) / 2;

            for (int i = 0; i < cycleEndR; i++)
            {
                for (int j = 0; j < cycleEndC; j++)
                {
                    var t1 = matrix[i][j];
                    var t2 = matrix[j][end - i];
                    var t3 = matrix[end - i][end - j];
                    var t4 = matrix[end - j][i];
                    matrix[i][j] = t4;
                    matrix[j][end - i] = t1;
                    matrix[end - i][end - j] = t2;
                    matrix[end - j][i] = t3;
                }
            }
        }

        public void Rotate(int[] nums, int k)
        {
            if (k == 0 || nums.Length == 1) return;
            var current = nums[0];
            var start = 0;
            var it = 0;
            var n = nums.Length;
            if (k == n) return;
            k %= n;
            var t = 0;
            var jumpTo = k;

            while (it < n)
            {
                t = nums[jumpTo];
                nums[jumpTo] = current;
                current = t;

                if (jumpTo == start)
                {
                    jumpTo++;
                    start = jumpTo;
                    current = nums[jumpTo];
                }

                jumpTo = (jumpTo + k) % n;




                it++;
            }

        }

        public int Jump(int[] nums)
        {
            if (nums.Length == 1) return 0;


            var currentRes = 0;
            var buildArr = new int[nums.Length];
            var n = nums.Length - 1;

            for (int i = n - 1; i >= 0; i--)
            {
                var dist = n - i;
                var stepsAllowed = nums[i];
                var stepsHere = 0;
                if (stepsAllowed >= dist)
                {
                    stepsHere = 1;

                }

                else if (stepsAllowed == 1)
                {
                    stepsHere = buildArr[i + 1] == 0
                        ? 0
                        : buildArr[i + 1] + 1;
                }

                else
                {
                    var res = int.MaxValue;
                    for (int j = 0; j < stepsAllowed; j++)
                    {
                        currentRes = buildArr[i + j + 1];
                        if (currentRes == 0) continue;

                        res = Math.Min(res, currentRes);

                    }
                    stepsHere = res == int.MaxValue
                        ? 0
                        : res + 1;
                }

                buildArr[i] = stepsHere;

            }


            return buildArr[0];

        }

        public int FindPaths(int m, int n, int maxMove, int startRow, int startColumn)
        {
            int M = 1000000000 + 7;
            var res = 0;
            var dp = new int[m][];
            for (int i = 0; i < dp.Length; i++)
            {
                dp[i] = new int[n];
            }

            dp[startRow][startColumn] = 1;

            for (int moves = 1; moves <= maxMove; moves++)
            {
                var t = new int[m][];
                for (int i = 0; i < t.Length; i++)
                {
                    t[i] = new int[n];
                }

                for (int i = 0; i < m; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (i == m - 1) res = (res + dp[i][j]) % M;
                        if (j == n - 1) res = (res + dp[i][j]) % M;
                        if (i == 0) res = (res + dp[i][j]) % M;
                        if (j == 0) res = (res + dp[i][j]) % M;

                        t[i][j] =
                            (((i > 0 ? dp[i - 1][j] : 0) +
                            (i < m - 1 ? dp[i + 1][j] : 0)) % M +
                            ((j > 0 ? dp[i][j - 1] : 0) +
                            (j < n - 1 ? dp[i][j + 1] : 0)) % M) % M;
                    }
                }
                dp = t;
            }


            return res;
        }

        public int RemoveDuplicates(int[] nums)
        {
            if (nums.Length < 2) return nums.Length;

            var current = nums[0];
            var nextIndex = 1;

            for (int i = 1; i < nums.Length; i++)
            {
                if (nums[i] == current) continue;

                nums[nextIndex] = nums[i];
                current = nums[i];
                nextIndex++;
            }

            return nextIndex;

        }

        public bool ContainsDuplicate(int[] nums)
        {
            var set = new HashSet<int>(nums);
            var s1 = set.Count;
            var s2 = nums.Length;

            return s1 != s2;
        }

        public bool ContainsNearbyDuplicate(int[] nums, int k)
        {
            if (k == 0 || nums.Length == 1) return false;
            var set = new HashSet<int>();



            for (int i = 0; i < nums.Length; i++)
            {
                if (set.Contains(nums[i])) return true;

                set.Add(nums[i]);

                if (i >= k)
                    set.Remove(nums[i - k]);

            }

            return false;
        }

        public bool ContainsNearbyDuplicate2(int[] nums, int k)
        {
            var dd = new Dictionary<int, int>();


            for (int i = 0; i < nums.Length; i++)
            {
                if (dd.ContainsKey(nums[i]))
                {
                    if ((Math.Abs(dd[nums[i]] - i)) <= k)
                        return true;

                    dd.Remove(nums[i]);
                }


                dd.Add(nums[i], i);
            }

            return false;
        }

        public int[] PlusOne(int[] digits)
        {
            var i = digits.Length - 1;
            var last = digits[i] + 1;
            digits[i] = last % 10;
            var add = last / 10;

            i--;
            while (add != 0 && i >= 0)
            {

                last = digits[i] + 1;
                digits[i] = last % 10;
                add = (last) / 10;
                i--;
            }

            if (add == 1)
            {
                var newDigits = new List<int>(digits.Length + 1);
                newDigits.Add(1);
                newDigits.AddRange(digits);
                digits = newDigits.ToArray();
            }

            return digits;
        }


        public void DuplicateZeros(int[] arr)
        {
            var l = arr.ToList();

            var i = 0;
            while (i < arr.Length)
            {

                if (l[i] == 0)
                {
                    l.Insert(i + 1, 0);
                    i++;
                }

                i++;
            }


            for (int j = 0; j < arr.Length; j++)
            {
                arr[j] = l[j];
            }
        }

        public void DuplicateZeros2(int[] arr)
        {
            var queue = new Queue<int>();

            var i = 0;
            while (i < arr.Length)
            {
                var current = arr[i];
                if (queue.Any())
                {
                    queue.Enqueue(current);
                    arr[i] = queue.Dequeue();
                }
                if (current == 0)
                {
                    queue.Enqueue(0);

                }

                i++;
            }


        }

        public int[] TwoSum(int[] nums, int target)
        {
            var res = new List<int>();

            for (int i = 0; i < nums.Length - 1; i++)
            {
                var current = target - nums[i];

                for (int j = i + 1; j < nums.Length; j++)
                {
                    if (current - nums[j] == 0)
                    {
                        res.Add(i);
                        res.Add(j);
                        return res.ToArray();
                    }
                }
            }

            return null;
        }

        public void SetZeroes(int[][] matrix)
        {
            var zerosRow = new List<int>();
            var zerosCol = new List<int>();


            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[0].Length; j++)
                {
                    if (matrix[i][j] == 0)
                    {
                        zerosRow.Add(i);
                        zerosCol.Add(j);
                    }
                }
            }

            foreach (var zero in zerosRow)
            {
                for (int j = 0; j < matrix[0].Length; j++)
                {
                    matrix[zero][j] = 0;
                }
            }

            foreach (var zero in zerosCol)
            {
                for (int i = 0; i < matrix.Length; i++)
                {
                    matrix[i][zero] = 0;
                }
            }
        }


        public IList<IList<string>> GroupAnagrams(string[] strs)
        {

            Dictionary<char, int> BuildAnagramDict(string s2)
            {
                var dd = new Dictionary<char, int>();

                foreach (var c in s2)
                {
                    if (!dd.ContainsKey(c))
                        dd.Add(c, 0);

                    dd[c]++;
                }

                return dd;
            }

            var res = new List<IList<string>>();

            for (int i = 0; i < strs.Length; i++)
            {
                if (strs[i] == "0") continue;
                var current = strs[i];
                var currentDD = BuildAnagramDict(current);

                var currentList = new List<string>() { current };

                for (int j = i + 1; j < strs.Length; j++)
                {
                    if (strs[j] == "0" ||
                        strs[j].Length != current.Length)
                        continue;

                    var ddHere = BuildAnagramDict(strs[j]);

                    if (currentDD.Except(ddHere).Any())
                        continue;

                    currentList.Add(strs[j]);
                    strs[j] = "0";
                }

                res.Add(currentList);
            }

            return res;
        }

        public IList<IList<string>> GroupAnagrams2(string[] strs)
        {

            var dd = new Dictionary<string, List<string>>();

            var res = new List<IList<string>>();

            foreach (var item in strs)
            {
                var a = String.Concat(item.OrderBy(c => c));
                if (!dd.ContainsKey(a))
                    dd.Add(a, new List<string>());
                
                dd[a].Add(item);
            }

            foreach (var (k, v) in dd)
            {
                res.Add(v);
            }

            return res;
        }

        public string Some()
        {
            var a = new Dictionary<char, int>(){
                {'a', 1},
                {'b', 2}
            };
            var b = new Dictionary<char, int>(){
                {'a', 1},
                {'b', 2}
            };

            var n = a.Except(b);

            return n.ToString();
        }
    }

    class SequenceComparer<T> : IEqualityComparer<IEnumerable<T>>
    {
        public bool Equals(IEnumerable<T> seq1, IEnumerable<T> seq2)
        {
            return seq1.SequenceEqual(seq2);
        }

        public int GetHashCode(IEnumerable<T> seq)
        {
            int hash = 1234567;
            foreach (T elem in seq)
                hash = hash * 37 + elem.GetHashCode();
            return hash;
        }
    }



}