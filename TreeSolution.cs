

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace leetc
{



    public class TreeNode
    {
        public int val;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public class TreeSolution
    {
        public int RangeSumBST(TreeNode root, int low, int high)
        {
            if (root == null) return 0;
            var res = 0;

            if (root.val <= high && root.val >= low)
            {
                res += root.val;
            }

            if (root.val > low) res += RangeSumBST(root.left, low, high);
            if (root.val < high) res += RangeSumBST(root.right, low, high);

            return res;
        }

        public int RangeSumBST2(TreeNode root, int low, int high)
        {
            int BFS(TreeNode r)
            {
                if (r == null) return 0;

                var res = 0;

                if (r.val <= high && r.val >= low)
                {
                    res += r.val;
                }

                return res + BFS(r.right) + BFS(r.left);
            }



            return BFS(root);

        }

        public int MaxDepth(TreeNode root)
        {
            if (root == null) return 0;


            var left = 1 + MaxDepth(root.left);
            var right = 1 + MaxDepth(root.right);

            return left > right ? left : right;
        }

        public int MaxDepth1(TreeNode root)
        {
            var res = 0;

            int Check(TreeNode node)
            {
                if (node == null) return 0;

                var sum = 1;

                sum = Math.Max((sum + Check(node.left)), (sum + Check(node.right)));


                return sum;
            }

            res = Check(root);

            return res;
        }

        public int MaxDepth_bfs(TreeNode root)
        {
            var bfsQueue = new Queue<TreeNode>();

            var res = 0;
            if (root == null) return 0;


            bfsQueue.Enqueue(root);

            while (bfsQueue.Any())
            {
                var count = bfsQueue.Count;
                for (int i = 0; i < count; i++)
                {
                    var n = bfsQueue.Dequeue();
                    if (n.right != null) bfsQueue.Enqueue(n.right);
                    if (n.left != null) bfsQueue.Enqueue(n.left);
                }



                res++;

            }



            return res;
        }

        public IList<int> InorderTraversal1(TreeNode root)
        {
            var res = new List<int>();

            var stack = new Stack<TreeNode>();

            while (stack.Any() || root != null)
            {
                if (root != null)
                {
                    stack.Push(root);
                    root = root.left;
                }
                else
                {

                    root = stack.Pop();
                    res.Add(root.val);
                    root = root.right;

                }
            }
            return res;
        }

        public IList<int> InorderTraversa(TreeNode root)
        {
            var res = new List<int>();

            void DFS(TreeNode node)
            {
                if (node == null) return;

                if (node.left != null)
                {
                    DFS(node.left);
                }

                res.Add(node.val);

                DFS(node.right);
            }

            DFS(root);

            return res;
        }

        public IList<int> PreorderTraversal1(TreeNode root)
        {
            var res = new List<int>();

            var stack = new Stack<TreeNode>();

            while (stack.Any() || root != null)
            {
                if (root != null)
                {
                    res.Add(root.val);
                    stack.Push(root);
                    root = root.left;
                }
                else
                {

                    root = stack.Pop();
                    root = root.right;

                }
            }

            return res;
        }

        public IList<int> PreorderTraversal(TreeNode root)
        {
            var res = new List<int>();

            void DFS(TreeNode node)
            {
                if (node == null) return;

                res.Add(node.val);


                DFS(node.left);




                DFS(node.right);
            }

            DFS(root);

            return res;
        }

        public IList<int> PostorderTraversal1(TreeNode root)
        {
            var res = new List<int>();

            var stack = new Stack<TreeNode>();

            while (stack.Any() || root != null)
            {
                if (root != null)
                {
                    if (root.right != null) stack.Push(root.right);
                    stack.Push(root);
                    root = root.left;
                }
                else
                {
                    root = stack.Pop();
                    if (root.right != null && stack.Any() && root.right == stack.Peek())
                    {
                        var t = root;
                        root = root.right;
                        stack.Pop();
                        stack.Push(t);
                    }
                    else
                    {
                        res.Add(root.val);
                        root = null;
                    }
                }
            }

            return res;
        }

        public IList<int> PostorderTraversal(TreeNode root)
        {
            var res = new List<int>();

            void DFS(TreeNode node)
            {
                if (node == null) return;


                DFS(node.left);

                if (node.right != null)
                {
                    DFS(node.right);
                }
                res.Add(node.val);
            }

            DFS(root);

            return res;
        }


        public int MaxDepth_dfs(TreeNode root)
        {

            if (root == null) return 0;

            var stack = new Stack<TreeNode>();

            var res = 0;
            var current = 0;

            while (stack.Any() || root != null)
            {
                res = Math.Max(res, current);
                if (root != null)
                {
                    if (root.right != null) stack.Push(root.right);
                    current++;
                    stack.Push(root);
                    root = root.left;
                }
                else
                {
                    root = stack.Pop();
                    current--;
                    if (root.right != null && stack.Any() && root.right == stack.Peek())
                    {
                        var t = root;
                        root = root.right;
                        stack.Pop();
                        stack.Push(t);
                    }
                    else
                    {
                        current--;
                        root = null;
                    }
                }
            }

            return res;
        }

        public IList<TreeNode> AllPossibleFBT1(int n)
        {
            if (n % 2 == 0) return new List<TreeNode>();
            if (n == 1) return new List<TreeNode>() { new TreeNode() };


            var dd = new Dictionary<int, List<TreeNode>>(){
                {
                    1,
                    new List<TreeNode>()
                    {
                        new TreeNode()
                    }
                }
            };

            IList<TreeNode> DoMo(int n)
            {
                if (dd.ContainsKey(n)) return dd[n];
                var res = new List<TreeNode>();

                for (int i = 1; i < n; i += 2)
                {
                    var leftnodes = DoMo(i);
                    var rightnodes = DoMo(n - i - 1);

                    foreach (var l in leftnodes)
                    {
                        foreach (var r in rightnodes)
                        {
                            var r1 = new TreeNode();
                            r1.left = l;
                            r1.right = r;
                            res.Add(r1);
                        }
                    }
                }
                dd.Add(n, res);
                return res;
            }


            DoMo(n);

            return dd[n];
        }

        public IList<TreeNode> AllPossibleFBT(int n)
        {
            var res = new List<TreeNode>();
            if (n % 2 == 0) return res;

            var one = new TreeNode();
            if (n == 1)
            {
                res.Add(one);
                return res;
            }



            for (int i = 1; i < n; i += 2)
            {
                var leftnodes = AllPossibleFBT(i);
                var rightnodes = AllPossibleFBT(n - i - 1);

                foreach (var lefty in leftnodes)
                {
                    foreach (var right in rightnodes)
                    {
                        var r = new TreeNode();
                        r.left = lefty;
                        r.right = right;
                        res.Add(r);
                    }
                }
            }



            return res;

        }

        public bool IsSymmetric(TreeNode root)
        {
            var res = true;

            void DFS(TreeNode node, TreeNode mirror)
            {
                if (node == null && mirror == null) return;
                if (node == null || mirror == null || node.val != mirror.val)
                {
                    res = false;
                    return;
                }

                DFS(node.left, mirror.right);




                DFS(node.right, mirror.left);
            }

            DFS(root.left, root.right);

            return res;
        }

        public TreeNode InvertTree(TreeNode root)
        {
            if (root == null) return null;
            void DFS(TreeNode node)
            {
                if (node == null) return;
                if (node.left == null && node.right == null) return;

                var t = node.left;
                node.left = node.right;
                node.right = t;

                DFS(node.left);
                DFS(node.right);
            }

            DFS(root);

            return root;
        }


         public TreeNode MergeTrees(TreeNode root1, TreeNode root2)
        {

            TreeNode preorder(TreeNode t1, TreeNode t2)
            {
                if (t1 == null && t2 == null) return null;

                var v1 = 0;
                var v2 = 0;
                if (t1 != null) v1 = t1.val;
                else t1 = new TreeNode();
                if (t2 != null) v2 = t2.val;
                else t2 = new TreeNode();

                var newT = new TreeNode(v1 + v2);
                newT.left = preorder(t1.left, t2.left);
                newT.right = preorder(t1.right, t2.right);

                return newT;
            }

            return preorder(root1, root2);
        }

        public TreeNode MergeTrees_BFS(TreeNode root1, TreeNode root2)
        {
            TreeNode checkIt(TreeNode t)
            {
                return t == null ? new TreeNode() : t;
            }
            var queue = new Queue<TreeNode>();
            var resQ = new Queue<TreeNode>();

            var res = new TreeNode();
            var current = res;
            current.right = new TreeNode();

            if (root1 == null) return root2;
            if (root2 == null) return root1;

            queue.Enqueue(root1);
            queue.Enqueue(root2);

            resQ.Enqueue(current.right);

            while (queue.Any())
            {
                var t1 = queue.Dequeue();
                var t2 = queue.Dequeue();
                var sum = t1.val + t2.val;

                var c = resQ.Dequeue();
                c.val = sum;

                if (t1.left != null || t2.left != null)
                {
                    queue.Enqueue(checkIt(t1.left));
                    queue.Enqueue(checkIt(t2.left));

                    c.left = new TreeNode();
                    resQ.Enqueue(c.left);
                }
                if (t1.right != null || t2.right != null)
                {
                    queue.Enqueue(checkIt(t1.right));
                    queue.Enqueue(checkIt(t2.right));

                    c.right = new TreeNode();
                    resQ.Enqueue(c.right);
                }
            }



            return res.right;
        }
        
        public TreeNode IncreasingBST(TreeNode root)
        {
            var res = new TreeNode();
            var stack = new Stack<TreeNode>();

            var prev = res;

            while (stack.Any() || root != null)
            {
                if (root != null)
                {
                    stack.Push(root);
                    root = root.left;
                }
                else
                {
                    root = stack.Pop();
                    prev.right = root;
                    prev.left = null;
                    prev = prev.right;
                    root = root.right;
                }
            }

            return res.right;


        }

        public TreeNode IncreasingRec(TreeNode root)
        {
            if (root == null) return null;

            TreeNode left = null;
            var right = root.right;
            if (root.left != null)
            {
                var tl = IncreasingRec(root.left);
                left = tl;
            }

            right = IncreasingRec(root.right);


            var temp = left;
            temp.right = new TreeNode();
            temp = temp.right;

            while (temp != null)
            {
                temp = temp.right;
            }

            if (left != null)
            {
                temp = root;
                temp.right = right;
                root = left;
            }



            return root;


        }

       
    }
}
