


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace leetc
{
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }

    public class LinkedListSolution
    {
        public ListNode MergeTwoLists(ListNode l1, ListNode l2)
        {
            if (l1 == null) return l2;
            if (l2 == null) return l1;

            if (l1.val < l2.val)
            {
                l1.next = MergeTwoLists(l1.next, l2);
                return l1;
            }

            else
            {
                l2.next = MergeTwoLists(l1, l2.next);
                return l2;
            }
        }

        public ListNode MergeTwoLists1(ListNode l1, ListNode l2)
        {
            var res = new ListNode();
            var tempNode1 = res;

            while (l1 != null && l2 != null)
            {

                var tempNode = new ListNode();
                if (l1.val < l2.val)
                {
                    tempNode.val = l1.val;
                    l1 = l1.next;
                }
                else
                {
                    tempNode.val = l2.val;
                    l2 = l2.next;
                }


                tempNode1.next = tempNode;
                tempNode1 = tempNode1.next;
            }

            tempNode1.next = l1 == null ? l2 : l1;

            return res.next;


        }

        public ListNode MergeTwoLists2(ListNode l1, ListNode l2)
        {
            var res = new ListNode();
            var tempNode = res;

            while (l1 != null && l2 != null)
            {


                if (l1.val < l2.val)
                {
                    tempNode.next = l1;
                    l1 = l1.next;
                }
                else
                {
                    tempNode.next = l2;
                    l2 = l2.next;
                }


                tempNode = tempNode.next;
            }

            tempNode.next = l1 == null ? l2 : l1;

            return res.next;
        }

        public ListNode GetIntersectionNode1(ListNode headA, ListNode headB)
        {
            while (headA != null)
            {
                var t = headB;
                while (headB != null)
                {
                    if (headA == headB)
                    {
                        return headA;
                    }
                    headB = headB.next;
                }
                headB = t;
                headA = headA.next;
            }

            return null;
        }

        public ListNode GetIntersectionNode2(ListNode headA, ListNode headB)
        {
            var lenA = 0;
            var lenB = 0;
            var A = headA;
            var B = headB;

            while (headA != null)
            {
                lenA++;
                headA = headA.next;
            }
            headA = A;

            while (headB != null)
            {
                lenB++;
                headB = headB.next;
            }
            headB = B;

            var diff = Math.Abs(lenA - lenB);

            if (diff != 0 && lenA > lenB)
            {
                for (int i = 0; i < diff; i++)
                {
                    headA = headA.next;
                }
            }
            else if (diff != 0)
            {
                for (int i = 0; i < diff; i++)
                {
                    headB = headB.next;
                }
            }

            while (headA != headB)
            {
                headA = headA.next;
                headB = headB.next;
            }

            return headA;
        }

        public ListNode MiddleNode(ListNode head)
        {
            var fast = head;
            var slow = head;

            while (fast != null && fast.next != null)
            {
                slow = slow.next;
                fast = fast.next.next;
            }

            return slow;
        }




        public ListNode SortList(ListNode head)
        {
            if (head == null) return null;
            if (head.next == null) return head;

            var fast = head;
            var slow = head;

            while (fast.next != null && fast.next.next != null)
            {
                slow = slow.next;
                fast = fast.next.next;
            }

            var t = slow.next;
            slow.next = null;
            var a = SortList(head);
            var b = SortList(t);


            return MergeTwoLists(a, b);
        }



        public ListNode SortList_iter(ListNode head)
        {
            var stackReady = new Stack<ListNode>();
            var stackToSort = new Stack<ListNode>();

            while (true)
            {
                var sd = SplitByElement(head, (FindLListLength(head) / 2) + 1);
                head = sd.Item1;
                var middle = sd.Item2;

                if (head.next != null)
                {
                    stackToSort.Push(middle);
                    continue;
                }

                var s = MergeTwoLists2(head, middle);
                if (stackReady.Any())
                {
                    var ssec = stackReady.Pop();
                    stackReady.Push(MergeTwoLists2(s, ssec));
                }
                else
                {
                    stackReady.Push(s);
                }

                head = stackToSort.Pop();
            }


            return null;
        }

        private int FindLListLength(ListNode head)
        {
            var i = 0;

            while (head != null)
            {
                i++;
                head = head.next;
            }
            return i;
        }

        private (ListNode, ListNode) SplitByElement(ListNode head, int splitPoint)
        {
            var f = head;
            for (int i = 0; i < splitPoint - 1; i++)
            {
                head = head.next;
            }

            var t = head.next;
            head.next = null;

            return (f, t);
        }

        public ListNode RemoveNthFromEnd(ListNode head, int n)
        {
            if (head.next == null && n == 1) return null;
            var slow = head;
            var fast = head;

            for (int i = 0; i < n; i++)
            {
                fast = fast.next;
            }

            if (fast == null) return head.next;

            while (fast.next != null)
            {
                slow = slow.next;
                fast = fast.next;
            }

            var t = slow.next.next;
            slow.next = t;

            return head;
        }

        public ListNode AddTwoNumbers(ListNode l1, ListNode l2)
        {
            var r = new ListNode();
            var t = r;

            var pend = 0;
            while (l1 != null || l2 != null)
            {


                var l1v = l1 == null ? 0 : l1.val;
                var l2v = l2 == null ? 0 : l2.val;

                var sum = (pend + l1v + l2v);
                pend = sum / 10;

                t.next = new ListNode(sum % 10);
                t = t.next;

                l1 = l1 == null ? null : l1.next;
                l2 = l2 == null ? null : l2.next;
            }

            if (pend == 1) t.next = new ListNode(1);

            return r.next;
        }

        public ListNode ReverseBetween(ListNode head, int left, int right)
        {
            if (left == right) return head;
            var res = head;

            ListNode toAttach = null;
            var toReverse = head;
            var toContinue = new ListNode();

            var count = 2;

            while (true)
            {
                if (count == left)
                {
                    toReverse = head.next;
                    toAttach = head;
                }
                if (count - 1 == right)
                {
                    if (head.next == null && left == 1) return ReverseList(res, null);
                    toContinue = head.next;
                    head.next = null;
                    break;
                }
                head = head.next;
                count++;
            }

            if (toAttach == null) return ReverseList(toReverse, toContinue);
            toAttach.next = ReverseList(toReverse, toContinue);

            return res;
        }

        private ListNode ReverseList(ListNode head, ListNode next)
        {
            var res = new ListNode();
            var t = res;

            ListNode Doo(ListNode n)
            {
                if (n == null || n.next == null)
                {
                    t.next = n;
                    return n;
                }

                var tt = Doo(n.next);

                tt.next = n;
                tt = tt.next;
                tt.next = null;


                return tt;
            }

            var end = Doo(head);
            end.next = next;

            return res.next;
        }

        public ListNode ReverseList(ListNode head)
        {
            var res = new ListNode();
            var t = res;

            ListNode Doo(ListNode n)
            {
                if (n == null || n.next == null)
                {
                    t.next = n;
                    return n;
                }

                var tt = Doo(n.next);

                tt.next = n;
                tt = tt.next;
                tt.next = null;


                return tt;
            }

            var nothing = Doo(head);

            return res.next;
        }

        public ListNode ReverseList2(ListNode head)
        {
            if (head == null || head.next == null) return head;

            var res = ReverseList2(head.next);
            head.next.next = head;
            head.next = null;

            return res;
        }

        
    }
}
